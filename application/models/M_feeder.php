<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_feeder extends CI_Model {

	public function call_soap()
	{
		include APPPATH.'third_party/nusoap/nusoap.php';
		include APPPATH.'third_party/nusoap/class.wsdlcache.php';

		$client = new nusoap_client(config_feeder('url').'/ws/live.php?wsdl', true);
		return $client->getProxy();
	}

	public function id_sp($kode_pt,$proxy)
	{
		$gettoken = token();
		$sp=$proxy->GetRecord($gettoken,'satuan_pendidikan',"npsn='$kode_pt'");
		$sp = json_decode(json_encode($sp['result']));
		return $sp->id_sp;
	}

}

/* End of file M_feeder.php */
/* Location: ./application/models/M_feeder.php */