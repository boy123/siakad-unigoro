
<div class="row">
    <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-blue">
                <span class="widget-caption"><?php echo $judul_page; ?></span>
            </div>
        <div class="widget-body">
        <div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Judul <?php echo form_error('judul') ?></label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul" value="<?php echo $judul; ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Tujuan <?php echo form_error('tujuan') ?></label>
            <select name="tujuan" id="level" style="width:100%;">
                <option value="">--Pilih Level --</option>
                <?php 
                foreach ($this->db->get('level')->result() as $rw): 
                    $checked = ($tujuan == $rw->id_level) ? 'selected' : '' ;
                    ?>
                    <option value="<?php echo $rw->id_level ?>" <?php echo $checked ?>><?php echo $rw->level ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <div class="form-group">
            <label>Publish</label><br>
            <label>
                <input class="checkbox-slider yesno colored-blue" name="aktif" type="checkbox" <?php echo ($aktif == '1') ? 'checked' : '' ?>>
                <span class="text"></span>
            </label>
        </div>
	    <div class="form-group">
            <label for="isi">Isi <?php echo form_error('isi') ?></label>
            <textarea class="form-control textarea_editor" rows="3" name="isi" id="isi" placeholder="Isi"><?php echo $isi; ?></textarea>
        </div>
	    <input type="hidden" name="id_pengumuman" value="<?php echo $id_pengumuman; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('pengumuman') ?>" class="btn btn-default">Cancel</a>
	</form>
                                    </div>
                                </div>
                                </div>

<script type="text/javascript" src="assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: ".textarea_editor",
        height: "500",
        plugins: [
             "advlist autolink link image lists charmap print preview hr anchor pagebreak fullscreen",
             "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
             "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
       ],
       toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
       toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
       image_advtab: true ,
       
       external_filemanager_path:"assets/filemanager/",
       filemanager_title:"Responsive Filemanager" ,
       external_plugins: { "filemanager" : "<?php echo base_url() ?>assets/filemanager/plugin.min.js"}
   });

    

</script>
   