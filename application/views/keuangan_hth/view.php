
<script type="text/javascript">

	function editEndDate(nomor_pembayaran) {
		// var nomor_pembayaran = $(this).attr("data-no");
		var waktu_berakhir = $("#waktu_berakhir"+nomor_pembayaran).val();
		$.ajax({
			url: 'Keuangan_hth/edit_tagihan',
			type: 'POST',
			dataType: 'html',
			data: {nomor_pembayaran: nomor_pembayaran, waktu_berakhir: waktu_berakhir},
		})
		.done(function() {
			console.log("success");
			// window.location.reload();
			alert("berhasil di edit !");
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	}


	function editTotalTagihan(nomor_pembayaran) {
		// alert(nomor_pembayaran);
		// var nomor_pembayaran = $(this).attr("data-no");
		var total_nilai_tagihan = $("#total_nilai_tagihan"+nomor_pembayaran).val();
		// alert(total_nilai_tagihan);
		$.ajax({
			url: 'Keuangan_hth/edit_tot_tagihan',
			type: 'POST',
			dataType: 'html',
			data: {nomor_pembayaran: nomor_pembayaran, total_nilai_tagihan: total_nilai_tagihan},
		})
		.done(function() {
			console.log("success");
			// window.location.reload();
			alert("berhasil di edit !");
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	}


		
</script>

<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Filter</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">
            <div class="row">
			<form action="">
				<div class="col-md-3">
					<select name="prodi" class="form-control">
						<option value="">Pilih Prodi</option>
						<?php foreach ($this->db->get('prodi')->result() as $key => $value): ?>
							<option value="<?php echo $value->kode_prodi ?>"><?php echo $value->prodi ?></option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-md-2">
					<input type="text" name="nim" class="form-control" placeholder="Masukkan Nim">
				</div>
				<div class="col-md-2">
					<input type="text" name="tahun" class="form-control" placeholder="EX: 2019" required>
				</div>
				<div class="col-md-1">
					<button class="btn btn-primary">Cari</button>
				</div>

				<div class="col-md-3">
					<a class="btn btn-info" data-toggle="modal" data-target="#modalImport">Import Tagihan</a>

					
				</div>
			</form>
			</div>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">PMB</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">
            <div class="row">
			<form action="keuangan_hth/pmb?aksi">
				
				<div class="col-md-2">
					<input type="text" name="tahun" class="form-control" placeholder="EX: 2021" required>
				</div>
				<div class="col-md-1">
					<button class="btn btn-primary">Cetak</button>
				</div>

					
				</div>
			</form>
			</div>
            </div>
        </div>
    </div>
</div>


<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Daftar Keuangan</span>
                <div class="widget-buttons">

                    <?php if ($_GET): ?>
                        <a href="keuangan_hth/cetak_h2h?aksi=cetak&<?php echo param_get() ?>" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-print"></i> Cetak</a>
                        
                    <?php endif ?>
                </div>
            </div>
            <div class="widget-body bordered-left bordered-warning">

            	<?php if ($_GET): ?>
            		<a href="<?php echo $this->uri->segment(1)."/cetak_h2h?&prodi=".$_GET['prodi']."&nim=".$_GET['nim']."&tahun=".$_GET['tahun']."&aksi=cetak" ?>" class="btn btn-info">Cetak</a>
            	<?php endif ?>
                
                
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>

                <br>




                <div class="table-scrollable">
	                <table class="table table-bordered table-hover table-striped" id="searchable">
	                    <thead class="bordered-darkorange">
	                        <tr role="row">
	                            <th>No Pembayaran</th>
								<th>Nim</th>
								<th>Nama</th>
								<th>Prodi</th>
								<th>Kode Periode</th>
								<th>Periode</th>
								<th>Total Tagihan</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Status Bayar</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <?php if ($_GET): 
							$prodi = $this->input->get('prodi');
							$nim = $this->input->get('nim');
							$tahun = $this->input->get('tahun');

				        	$keuangan = $this->load->database('keuangan', TRUE);
				        	if ($prodi !== '') {
				        		$keuangan->where('kode_prodi', $prodi);
				        	}
				        	if ($nim !== '') {
				        		$keuangan->where('nomor_induk', $nim);
				        	}
				        	$keuangan->like('waktu_berlaku', $tahun, 'after');
				        	$keuangan->order_by('id', 'desc');
							$sql = $keuangan->get('tagihan');
							foreach ($sql->result() as $rw) {
						 ?>
	            
	                        <tr>
	                            <td><?php echo $rw->nomor_pembayaran; ?></td>
								<td><?php echo $rw->nomor_induk; ?></td>
								<td><?php echo $rw->nama; ?></td>
								<td><?php echo $rw->nama_prodi ?></td>
								<td><?php echo $rw->kode_periode; ?></td>
								<td><?php echo $rw->nama_periode; ?></td>
	                            <td style="width: 500px;">
	                            	<div class="input-group">
			                            <input class="form-control" name="total_nilai_tagihan" value="<?php echo $rw->total_nilai_tagihan; ?>" id="total_nilai_tagihan<?php echo $rw->nomor_pembayaran ?>" autocomplete="off">
			                            <span class="input-group-addon" onclick="editTotalTagihan(<?php echo $rw->nomor_pembayaran ?>)">
			                                <i class="fa fa-save"></i>
			                            </span>
			                        </div>
	                            </td>
	                            <td><?php echo $rw->waktu_berlaku; ?></td>
	                            <td style="width: 500px;">
	                            	<div class="input-group">
			                            <input class="form-control" name="waktu_berakhir" value="<?php echo $rw->waktu_berakhir; ?>" id="waktu_berakhir<?php echo $rw->nomor_pembayaran ?>" autocomplete="off">
			                            <span class="input-group-addon" onclick="editEndDate(<?php echo $rw->nomor_pembayaran ?>)" >
			                                <i class="fa fa-save"></i>
			                            </span>
			                        </div>
	                            </td>
	                            <td>
	                            	<?php 
									$cek_bayar = $keuangan->get_where('pembayaran', array('id_record_tagihan'=>$rw->id_record_tagihan));
									if ($cek_bayar->num_rows() > 0) {
										echo '<span class="label label-success">TERBAYAR</span>';
									} else {
										echo '<span class="label label-danger">BELUM DIBAYAR</span>';
									}
									 ?>
	                            </td>
	                            
	                            
	                        </tr>

	                    <?php } ?>

						<?php endif ?>
	                        
	                    </tbody>
	                </table>
            	</div>


            </div>
        </div>
    </div>
</div>





<!-- Modal -->
<div id="modalImport" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Import Tagihan H2H</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url('keuangan_hth/import_tagihan') ?>" method="POST" enctype="multipart/form-data">
        	<div class="form-group">
        		
        		<a href="excel/import_tagihan.xlsx"><label>Download Template</label></a>
        	</div>
        	<div class="form-group">
        		<label>Upload Excel</label>
        		<input type="file" name="file_excel" class="form-control">
        	</div>
        	<div class="form-group">
        		<button type="submit" class="btn btn-info">Upload</button>
        	</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


