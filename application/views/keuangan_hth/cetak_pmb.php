	<?php
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Rekap_Pembayaran_PMB.xls");
		?>
	<h2>REKAP PEMBAYARAN PMB</h2>
	<table class="table table-bordered" border="1">
			<thead>
			<tr class="alert-success">
				<td>Label</td>
				<td>No Pembayaran</td>
				<td>NIS</td>
				<td>Nama</td>
				<td>Total Tagihan</td>
				<td>Start Date</td>
				<td>End Date</td>
				<td>Status Bayar</td>
			</tr>
			</thead>
			<tbody>
			
			<?php 
				$no = 1;
	        	$keuangan = $this->load->database('keuangan', TRUE);
	        	$tahun = $this->input->get('tahun');
	        	$sql = "
	        		SELECT
						a.nomor_pembayaran,
						a.nama,
						a.nomor_induk,
						a.total_nilai_tagihan,
						b.label_jenis_biaya,
						a.waktu_berlaku,
						a.waktu_berakhir,
						c.waktu_transaksi,
						c.total_nilai_pembayaran
					FROM
						tagihan AS a
						LEFT JOIN pembayaran as c ON a.nomor_pembayaran=c.nomor_pembayaran
						INNER JOIN detil_tagihan AS b ON a.id_record_tagihan = b.id_record_tagihan
						AND b.label_jenis_biaya = 'PMB'
						AND a.waktu_berlaku LIKE '$tahun%'
						
					ORDER BY a.waktu_berlaku DESC
	        	 "
	        	?>

	        <?php foreach ($keuangan->query($sql)->result() as $rw): ?>
	        	<tr>
	        		<td><?php echo $no; ?></td>
	        		<td><?php echo $rw->nomor_pembayaran ?></td>
	        		<td><?php echo $rw->nomor_induk ?></td>
	        		<td><?php echo $rw->nama ?></td>
	        		<td><?php echo $rw->total_nilai_tagihan ?></td>
	        		<td><?php echo $rw->waktu_berlaku ?></td>
	        		<td><?php echo $rw->waktu_berakhir ?></td>
	        		<td>
	        			<?php 
	        			if ($rw->waktu_transaksi != '') {
	        				echo "Terbayar";
	        			} else {
	        				echo "Belum dibayar";
	        			}

	        			 ?>
	        		</td>
	        	</tr>
	        <?php $no++; endforeach ?>
	        	
			</tbody>
		</table>