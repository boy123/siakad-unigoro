<?php 
$detail = $d_mhs->row();
 ?>

<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Data Mahasiswa</span>
                <div class="widget-buttons">

                </div>
            </div>
            <div class="widget-body bordered-left bordered-warning">

            	<div class="row">
            		<div class="col-md-4" style="margin-bottom: 40px">
	            		<table class="table table-hover table-striped table-bordered">
	            			<tr>
	            				<td>Nim</td>
	            				<td>:</td>
	            				<td><?php echo $detail->nim ?></td>
	            			</tr>
	            			<tr>
	            				<td>Nama</td>
	            				<td>:</td>
	            				<td><b><?php echo $detail->nama ?></b></td>
	            			</tr>
	            		</table>
	            	</div>
	            	<div class="col-md-8">
	            		<button class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Tagihan</button>
	            	</div>
            	</div>

                <div class="table-scrollable">
	                <table class="table table-bordered table-hover table-striped">
	                    <thead class="bordered-darkorange">
	                        <tr role="row">
	                            <th>No.</th>
								<th>Jumlah Tagihan</th>
								<th>Jatuh Tempo</th>
								<th>Status</th>
								<th>Option</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	
	                    <?php 
	                    $no = 1;
	                    $total = 0;
	                    foreach ($this->db->get_where('tagihan_mahasiswa', ['id_mahasiswa'=>$detail->id_mahasiswa])->result() as $row): ?>
	                    	<tr>
	                    		<td><?php echo $no ?></td>
	                    		<td><?php echo number_format($row->qty); $total = $total + $row->qty ?></td>
	                    		<td><?php echo $row->jatuh_tempo ?></td>
	                    		<td><?php echo $retVal = ($row->status == 'paid') ? '<label class="label label-success">PAID</label>' : '<label class="label label-danger">UNPAID</label>' ; ?></td>
	                    		<td>
	                    			<a href="tagihan_mhs/hapus/<?php echo $row->id_tagihan ?>" title="Hapus" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
	                    			<a href="tagihan_mhs/lunas/paid/<?php echo $row->id_tagihan ?>" class="btn btn-xs btn-success">SET LUNAS</a>
	                    		</td>
	                    	</tr>
	                    <?php $no++; endforeach ?>
	                        
	                    </tbody>
	                    <tfoot>
	                    	<tr>
	                    		<td><b>Total</b></td>
	                    		<td><b><?php echo number_format($total) ?></b></td>
	                    	</tr>
	                    </tfoot>
	                </table>
            	</div>


            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah Tagihan</h4>
        </div>
        <div class="modal-body">
          <form action="tagihan_mhs/simpan_tagihan/<?php echo $detail->id_mahasiswa ?>" method="POST">
          	<div class="form-group">
          		<label>Jumlah Tagihan</label>
          		<input type="number" name="qty" class="form-control" required>
          	</div>
          	<div class="form-group">
          		<label>Jatuh Tempo</label>
          		<input type="date" name="jatuh_tempo" class="form-control" required>
          	</div>
          	<div class="form-group">
          		<button type="submit" class="btn btn-info">Simpan</button>
          	</div>
          	<input type="hidden" name="id_mahasiswa" value="<?php echo $detail->id_mahasiswa ?>">
          	<input type="hidden" name="nim" value="<?php echo $detail->nim ?>">
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>