<!DOCTYPE html>
<html>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="image/logo.png">

        <title><?php echo $judul_page ?></title>

        <!-- Base Css Files -->
        <link href="assets/login/assets/css/bootstrap.min.css" rel="stylesheet" />

        <!-- Font Icons -->
        <link href="assets/login/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="assets/login/assets/plugins/ionicon/css/ionicons.min.css" rel="stylesheet" />
        <link href="assets/login/assets/css/material-design-iconic-font.min.css" rel="stylesheet">

        <!-- sweet alerts -->
        <link href="assets/login/assets/plugins/sweet-alert/sweet-alert.min.css" rel="stylesheet">

        <!-- animate css -->
        <link href="assets/login/assets/css/animate.css" rel="stylesheet" />

        <!-- Waves-effect -->
        <link href="assets/login/assets/css/waves-effect.css" rel="stylesheet">

        <!-- Custom Files -->
        <link href="assets/login/assets/css/helper.css" rel="stylesheet" type="text/css" />
        <link href="assets/login/assets/css/style.css" rel="stylesheet" type="text/css" />

        <link href="assets/login/assets/css/userdefined.css" rel="stylesheet" />
        <link href="assets/login/assets/css/login.css" rel="stylesheet" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://e-monev.bappenas.go.id/emon3/assets/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://e-monev.bappenas.go.id/emon3/assets/https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/login/assets/js/modernizr.min.js"></script>
        
    </head>
    <body style="background: url('assets/login/bg.jpeg') no-repeat center center fixed;background-size: cover;">
        <div class="se-pre-con"></div>
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>

        <div class="container" >  
            <div class="col-lg-4 col-md-3 col-sm-2">&nbsp;</div>
            <div class="col-lg-4 col-md-6 col-sm-8">
                <div class="logo">
                    <!--<img src="https://s16.postimg.org/3wg150ysl/download.jpg"  alt="Logo"  >--> 
                    <img src="image/logo.png"  alt="Logo" style="margin: 0px 0 20px 0;max-height: 200px;" /> 
                </div>
                <div class="text-center text-uppercase">
                    <!--<span style="font-size: 13px;font-weight: bold">Kementerian Perencanaan dan Pembangunan Nasional</span>-->
                </div>
                
                <div class="row loginbox" style="width: 75%;margin: 20px auto;  ">                    
                    <div class="col-lg-12 text-center">
                        <span class="singtext" ><b><?php echo get_data('setting','id_setting','1','nama_kampus') ?></b></span>   
                    </div>
                    <form action="login/auth" method="POST">
                        <div class="col-lg-12 col-md-12 col-sm-12 input_wrapper" style="margin-top: 50px;">
                            <input class="form-control input-xs" type="text" name="username" placeholder="Username" > 
                        </div>
                        <div class="col-lg-12  col-md-12 col-sm-12 input_wrapper">
                            <input class="form-control input-xs" type="password" name="password" placeholder="Password" >
                        </div>

                        <div class="col-lg-12  col-md-12 col-sm-12 input_wrapper">
                            <div class="row">
                                <?php 
                                $a = rand(1,10);
                                $b = rand(1,10);
                                 ?>
                                <input type="hidden" value="<?php echo $a ?>" name="a">
                                <input type="hidden" value="<?php echo $b ?>" name="b">
                                <div class="col-md-8" style="margin-top: 30px; font-size: 13px;">Hasil Penjumlahan : <b><?php echo $a ?></b> + <b><?php echo $b ?></b> = </div>
                                <div class="col-md-4">
                                    <input class="form-control input-xs" type="text" name="hasil" >
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-12  col-md-12 col-sm-12" style="margin-top: 20px;">
                            <button type="submit" class="btn submitButton"><i class="fa fa-sign-in"></i>&nbsp;Sign in </a>
                        </div>  
                        <center>
                            <span>-- atau login dengan --</span>
                        </center>
                        <div class="col-lg-12  col-md-12 col-sm-12" style="margin-top: 20px;">
                            <a href="<?php echo $google_link ?>" class="btn submitButton" style="background-color: red"> <i class="fa fa-google"></i>&nbsp;Google </a> 
                        </div>                     
                    </form>
                </div>
                <!-- <a href="front/login/https://e-monev.bappenas.go.id/fe/jumper"><h5 style="color:#FFF;text-align:center;font-weight: normal; font-size: x-small;" onMouseOver="this.style.color='#EF9608';this.style.fontWeight='bold'" onMouseOut="this.style.color='#FFF';this.style.fontWeight='normal'"><i class="fa fa-undo"></i>&nbsp;Kembali Pilih Tahun Anggaran</h5></a>                -->
            </div>
            <div class="col-lg-4 col-md-3 col-sm-2 text-center">&nbsp;</div>
            <br/>&nbsp;<br/>&nbsp;<br/>&nbsp;<br/>&nbsp;<br/>&nbsp;<br>&nbsp;<br>
        </div>
        <footer class="footer_login clearfix">
            <small>
                <?php echo get_data('setting','id_setting','1','nama_kampus') ?> © Copyright <?php echo date('Y') ?>. All Rights Reserved.      
            </small>            
        </footer> <!--footer Section ends-->
        
    	<script>
            var resizefunc = [];
        </script>
    	<script src="assets/login/assets/js/jquery.min.js"></script>
        <script src="assets/login/assets/js/bootstrap.min.js"></script>
        <script src="assets/login/assets/js/waves.js"></script>
        <script src="assets/login/assets/js/wow.min.js"></script>
        <script src="assets/login/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="assets/login/assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/login/assets/plugins/jquery-detectmobile/detect.js"></script>
        <script src="assets/login/assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/login/assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script src="assets/login/assets/plugins/jquery-blockui/jquery.blockUI.js"></script>
        <script src="assets/login/assets/plugins/sweet-alert/sweet-alert.min.js"></script>
      
        <!--PLUGINS - s -->

        <script src="assets/login/assets/plugins/jquery-validation-1.15.0/dist/jquery.validate.min.js"></script>
        <script src="assets/login/assets/plugins/jquery-validation-1.15.0/dist/additional-methods.min.js"></script>

        <!--PLUGINS - e -->

        <!-- CUSTOM JS -->
        <script src="assets/login/assets/js/jquery.app.js"></script>
        
        <!--USERDEFINED-->
        <script src="assets/login/assets/js/userdefined/universal.js"></script>
        <script src="assets/login/assets/js/userdefined/login/login.js"></script>
        <script type="text/javascript">
            login.init();            $(window).load(function() {
                // Animate loader off screen
                $(".se-pre-con").fadeOut("slow");;
            });
        </script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript"><?php echo $this->session->userdata('message') ?></script>
    </body>

</html>