<?php 
$level = $this->session->userdata('level');
if ($level == '4') {
    $id_dosen = $this->session->userdata('keterangan');
    $disable_cek = "tidak";
} else {
    $id_dosen = $this->uri->segment(3);
    $disable_cek = "iya";
}

 ?>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Filter</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">
                <form class="form-inline" action="" method="get" role="form">

                    <div class="form-group">
                        <select name="periode" id="periode" style="width:100%;" required="">
                            <option value="">--Pilih Periode --</option>
                            <?php 
                            $this->db->order_by('kode_tahun', 'desc');
                            foreach ($this->db->get('tahun_akademik')->result() as $rw): 
                                ?>
                                <option value="<?php echo $rw->kode_tahun ?>"><?php echo $rw->kode_tahun.' - '. $rw->keterangan ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <select name="id_prodi" id="id_prodi" style="width:100%;" required="">
                            <option value="">--Pilih Prodi --</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <select name="kode_mk" id="kode_mk" style="width:100%;" required="">
                            <option value="">--Pilih MK --</option>
                            
                        </select>
                    </div>

                    <div class="form-group">
                        <select name="kelas" id="kelas" style="width:100%;">
                            <option value="">--Semua Kelas --</option>
                            
                        </select>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">FILTER</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php if ($_GET):
    
    $id_prodi = $this->input->get('id_prodi');
    $kode_mk = $this->input->get('kode_mk');
    $kelas = $this->input->get('kelas');
    $kode_semester = $this->input->get('periode');

    ?>

<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption"><?php echo $judul_page ?> [ <b>Periode</b> : <?php echo $kode_semester ?>, <b>Prodi</b> : <?php echo get_data('prodi','id_prodi',$id_prodi,'prodi') ?>, <b>MK</b> : <?php 
                $this->db->where('id_prodi', $id_prodi);
                $this->db->where('kode_mk', $kode_mk);
                echo strtoupper($this->db->get('matakuliah')->row()->nama_mk);
                 ?>, <b>Kelas</b> : <?php echo $kls = ($kelas !='') ? $kelas : 'Semua Kelas' ; ?> ]</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                <br>
                <a target="_blank" href="cetak/cetak_absen_kosong?<?php echo param_get() ?>" class="btn btn-info"><i class="fa fa-print"></i> Cetak Absen Kosong</a>
                <a target="_blank" href="cetak/cetak_hadir_dosen?<?php echo param_get() ?>" class="btn btn-info"><i class="fa fa-print"></i> Cetak Hadir Dosen</a>

                <a target="_blank" href="cetak/export_nilai_dosen/<?php echo $id_dosen ?>?<?php echo param_get() ?>" class="btn btn-info"><i class="fa fa-print"></i> Export Nilai</a>

                <br>

                <div class="table-scrollable">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th rowspan="2" style="text-align: center; vertical-align: middle;">No</th>
                                <th rowspan="2" style="text-align: center; vertical-align: middle;">Nim</th>
                                <th rowspan="2" style="text-align: center; vertical-align: middle;">Nama</th>
                                <th rowspan="2" style="text-align: center; vertical-align: middle;">MK</th>
                                <th colspan="4" style="text-align: center;">NILAI</th>
                                <th rowspan="2" style="text-align: center; vertical-align: middle;">Angka</th>
                                <th rowspan="2" style="text-align: center; vertical-align: middle;">Huruf</th>
                                <th rowspan="2" style="text-align: center; vertical-align: middle;">Indeks</th>
                                <th rowspan="2" style="text-align: center; vertical-align: middle;">Pilihan</th>
                            </tr>
                            <tr>
                                <th>Absen (10%)</th>
                                <th>Latihan (30%)</th>
                                <th>UTS (30%)</th>
                                <th>UAS (30%)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $readonly = '';
                            if (tahun_akademik_aktif('kode_tahun') != $kode_semester) {
                                $readonly = 'readonly';
                            } elseif ($disable_cek == "ya") {
                                $readonly = 'readonly';
                            }
                            $no = 1;
                            if ($kelas !='') {
                                $this->db->where('kelas', $kelas);
                            }
                            $this->db->where('id_dosen', $id_dosen);
                            $this->db->where('kode_semester', $kode_semester);
                            $this->db->where('id_prodi', $id_prodi);
                            $this->db->where('kode_mk', $kode_mk);
                            foreach ($this->db->get('krs')->result() as $rw): ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $rw->nim ?></td>
                                <td><?php echo get_data('mahasiswa','nim',$rw->nim,'nama') ?></td>
                                <td><?php echo $rw->nama_mk ?></td>
                                <td>
                                    <form action="krs/simpan_nilai_mahasiswa/<?php echo $rw->id_krs.'?'.param_get() ?>" method="POST">
                                    <input type="text" name="kehadiran" value="<?php echo $rw->kehadiran ?>" <?php echo $readonly ?> required>
                                </td>
                                <td>
                                    <input type="text" name="latihan" value="<?php echo $rw->latihan ?>" <?php echo $readonly ?> required>
                                </td>
                                <td>
                                    <input type="text" name="uts" value="<?php echo $rw->uts ?>" <?php echo $readonly ?> required>
                                </td>
                                <td>
                                    <input type="text" name="uas" value="<?php echo $rw->uas ?>" <?php echo $readonly ?> required>
                                </td>
                                <td><?php echo $rw->angka ?></td>
                                <td><?php echo $rw->huruf ?></td>
                                <td><?php echo $rw->indeks ?></td>
                                <td>
                                    <?php if ($readonly == ''): ?>
                                        
                                    
                                        <?php if ($rw->kehadiran != ''): ?>
                                            <button type="submit" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></button>
                                        <?php else: ?>
                                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i></button>
                                        <?php endif ?>

                                    <?php endif ?>
                                    </form>
                                </td>
                            </tr>
                            <?php $no++; endforeach ?>
                            
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>
</div>



<?php endif ?>

<script type="text/javascript">
    $(document).ready(function() {

        $("#periode").change(function() {
            var kode_tahun = $(this).val();
            $.ajax({url: "kelas_ajar/get_select_periode/"+kode_tahun+"/<?php echo $id_dosen ?>", 
                beforeSend: function(){
                    $(".loading-container").show();
                    $(".loader").show();
                },
                success: function(result){
                    $("#id_prodi").html(result);
                  console.log("success");
                },
                complete:function(data){
                    $(".loading-container").hide();
                    $(".loader").hide();
                }
            });
            
        });

        $("#id_prodi").change(function() {
            var id_prodi = $(this).val();
            var kode_tahun = $("#periode").val();
            $.ajax({url: "kelas_ajar/get_select_mk/"+id_prodi+'/'+kode_tahun+"/<?php echo $id_dosen ?>", 
                beforeSend: function(){
                    $(".loading-container").show();
                    $(".loader").show();
                },
                success: function(result){
                    $("#kode_mk").html(result);
                  console.log("success");
                },
                complete:function(data){
                    $(".loading-container").hide();
                    $(".loader").hide();
                }
            });
            
        });

        $("#kode_mk").change(function() {
            var kode_mk = $(this).val();
            var id_prodi = $("#id_prodi").val();
            var kode_tahun = $("#periode").val();
            $.ajax({url: "kelas_ajar/get_select_kelas/"+id_prodi+"/"+kode_mk+"/<?php echo $id_dosen ?>/"+kode_tahun, 
                beforeSend: function(){
                    $(".loading-container").show();
                    $(".loader").show();
                },
                success: function(result){
                    $("#kelas").html(result);
                  console.log("success");
                },
                complete:function(data){
                    $(".loading-container").hide();
                    $(".loader").hide();
                }
            });
            
        });

    });
</script>