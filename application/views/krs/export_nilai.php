<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Export_Nilai.xls");
?>

<table class="table table-bordered table-hover table-striped">
    <thead>
        <tr>
            <th rowspan="2" style="text-align: center; vertical-align: middle;">No</th>
            <th rowspan="2" style="text-align: center; vertical-align: middle;">Nim</th>
            <th rowspan="2" style="text-align: center; vertical-align: middle;">Nama</th>
            <th rowspan="2" style="text-align: center; vertical-align: middle;">MK</th>
            <th colspan="4" style="text-align: center;">NILAI</th>
            <th rowspan="2" style="text-align: center; vertical-align: middle;">Angka</th>
            <th rowspan="2" style="text-align: center; vertical-align: middle;">Huruf</th>
            <th rowspan="2" style="text-align: center; vertical-align: middle;">Indeks</th>
        </tr>
        <tr>
            <th>Absen (10%)</th>
            <th>Latihan (30%)</th>
            <th>UTS (30%)</th>
            <th>UAS (30%)</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $readonly = '';
        $id_prodi = $this->input->get('id_prodi');
	    $kode_mk = $this->input->get('kode_mk');
	    $kelas = $this->input->get('kelas');
	    $kode_semester = $this->input->get('periode');
        $id_dosen = $this->uri->segment(3);
        $no = 1;
        if ($kelas !='') {
            $this->db->where('kelas', $kelas);
        }
        $this->db->where('id_dosen', $id_dosen);
        $this->db->where('kode_semester', $kode_semester);
        $this->db->where('id_prodi', $id_prodi);
        $this->db->where('kode_mk', $kode_mk);
        foreach ($this->db->get('krs')->result() as $rw): ?>
        <tr>
            <td><?php echo $no ?></td>
            <td><?php echo $rw->nim ?></td>
            <td><?php echo get_data('mahasiswa','nim',$rw->nim,'nama') ?></td>
            <td><?php echo $rw->nama_mk ?></td>
            <td>
                <?php echo $rw->kehadiran ?>
            </td>
            <td>
                <?php echo $rw->latihan ?>
            </td>
            <td>
                <?php echo $rw->uts ?>
            </td>
            <td>
                <?php echo $rw->uas ?>
            </td>
            <td><?php echo $rw->angka ?></td>
            <td><?php echo $rw->huruf ?></td>
            <td><?php echo $rw->indeks ?></td>
        </tr>
        <?php $no++; endforeach ?>
        
    </tbody>
</table>