<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Import Data</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">
                <div>
                    <a href="files/template/import_khs_dosen.xlsx" class="label label-success">Download Template</a>
                </div><br>
                <form class="form-inline" action="" method="POST" enctype="multipart/form-data" role="form">
                    <div class="form-group">
                        <input type="file" name="file_excel" class="form-control" required>
                    </div>

                    
                    
                    <button type="submit" class="btn btn-primary">Import</button>
                </form>
            

            <br>
            <hr>

            <?php if (isset($sheet) && $sheet!=''): ?>
            <table class="table">
                <tr>
                    <th>No.</th>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Kode mk</th>
                    <th>Nama mk</th>
                    <th>Nilai Akhir</th>
                    <th>Kode Semester</th>
                </tr>

                <?php
                $no = 1;
                $color1 = '';
                $color2 = '';
                 foreach ($sheet as $rw):
                    $this->db->where('nim', $rw['A']);
                    $this->db->where('kode_mk', $rw['C']);
                    $this->db->where('kode_semester', $rw['F']);
                    $mhs = $this->db->get('krs');
                    if ($mhs->num_rows() == 0) {
                       $color1 = 'style="background: red"';
                    } else {
                        $color1 = '';
                    }

                  ?>
                <tr >
                    <td><?php echo $no; ?></td>
                    <td <?php echo $color1 ?>><?php echo $rw['A']; ?></td>
                    <td><?php echo $rw['B']; ?></td>
                    <td><?php echo $rw['C']; ?></td>
                    <td><?php echo $rw['D']; ?></td>
                    <td><?php echo $rw['E']; ?></td>
                    <td><?php echo $rw['F']; ?></td>
                </tr>
                <?php $no++; endforeach ?>

            </table>

            <br>
            <span style="color: red">
                *) tanda merah berarti data krs tidak di temukan
            </span>
            <br><br>
            
            <a href="krs/aksi_import_khs_dosen" class="btn btn-success">Proses</a>


            <?php endif ?>
            </div>

        </div>
    </div>
</div>