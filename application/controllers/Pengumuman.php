<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengumuman extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pengumuman_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pengumuman/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pengumuman/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pengumuman/index.html';
            $config['first_url'] = base_url() . 'pengumuman/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Pengumuman_model->total_rows($q);
        $pengumuman = $this->Pengumuman_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pengumuman_data' => $pengumuman,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Daftar Pengumuman',
            'konten' => 'pengumuman/pengumuman_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Pengumuman_model->get_by_id($id);
        if ($row) {
            $data = array(
                'judul_page' => 'Detail Pengumuman',
            'konten' => 'pengumuman/pengumuman_read',
		'id_pengumuman' => $row->id_pengumuman,
        'judul' => $row->judul,
		'tujuan' => $row->tujuan,
		'isi' => $row->isi,
		'aktif' => $row->aktif,
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pengumuman'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'Tambah Pengumuman',
            'konten' => 'pengumuman/pengumuman_form',
            'button' => 'Simpan',
            'action' => site_url('pengumuman/create_action'),
	    'id_pengumuman' => set_value('id_pengumuman'),
	    'judul' => set_value('judul'),
        'isi' => set_value('isi'),
	    'tujuan' => set_value('tujuan'),
	    'aktif' => set_value('aktif'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'judul' => $this->input->post('judul',TRUE),
        'isi' => $this->input->post('isi',TRUE),
		'tujuan' => $this->input->post('tujuan',TRUE),
		'aktif' => $this->input->post('aktif') != null ? '1' : '0',
	    );

            $this->Pengumuman_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil disimpan
                                    </div>');
            redirect(site_url('pengumuman'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Pengumuman_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'Update Pengumuman',
                'konten' => 'pengumuman/pengumuman_form',
                'button' => 'Ubah',
                'action' => site_url('pengumuman/update_action'),
		'id_pengumuman' => set_value('id_pengumuman', $row->id_pengumuman),
		'judul' => set_value('judul', $row->judul),
        'isi' => set_value('isi', $row->isi),
		'tujuan' => set_value('tujuan', $row->tujuan),
		'aktif' => set_value('aktif', $row->aktif),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pengumuman'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_pengumuman', TRUE));
        } else {
            $data = array(
		'judul' => $this->input->post('judul',TRUE),
        'isi' => $this->input->post('isi',TRUE),
		'tujuan' => $this->input->post('tujuan',TRUE),
		'aktif' => $this->input->post('aktif') != null ? '1' : '0',
	    );

            $this->Pengumuman_model->update($this->input->post('id_pengumuman', TRUE), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil diubah
                                    </div>');
            redirect(site_url('pengumuman'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Pengumuman_model->get_by_id($id);

        if ($row) {
            $this->Pengumuman_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-success fade in alert-radius-bordered alert-shadowed">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-info"></i>

                                        <strong>Info:</strong> Data Berhasil dihapus
                                    </div>');
            redirect(site_url('pengumuman'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pengumuman'));
        }
    }

    public function _rules() 
    {
    $this->form_validation->set_rules('judul', 'judul', 'trim|required');
	$this->form_validation->set_rules('tujuan', 'Tujuan', 'trim|required');
	$this->form_validation->set_rules('isi', 'isi', 'trim|required');

	$this->form_validation->set_rules('id_pengumuman', 'id_pengumuman', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Pengumuman.php */
/* Location: ./application/controllers/Pengumuman.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2021-02-07 10:22:33 */
/* https://jualkoding.com */