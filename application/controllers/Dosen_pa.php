<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen_pa extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->rbac->check_module_access();
	}

	
	public function index()
	{
		$data = array(
			'konten' => 'dosen_pa/view',
			'judul_page' => 'Daftar Mahasiswa Bimbingan',
		);
		$this->load->view('v_index',$data);
	}

	public function detail_krs()
	{
		$nim = $this->input->get('nim');
		$kode_semester = $this->input->get('kode_semester');

		$data = array(
			'konten' => 'dosen_pa/detail_krs',
			'judul_page' => "Detail KRS",
			'nim' => $nim,
			'kode_semester' => $kode_semester,
		);
		$this->load->view('v_index',$data);

	}

	public function aksi_simpan_penolakan()
	{
		$alasan_ditolak = $this->input->post('alasan_ditolak');
		$nim = $this->input->post('nim');
		$kode_semester = $this->input->post('kode_semester');
		$param = $this->input->post('param');

		$simpan = $this->db->insert('krs_ditolak', array(
			'nim' => $nim,
			'kode_semester' => $kode_semester,
			'alasan_ditolak'=>$alasan_ditolak,
			'created_at' => get_waktu()
		));
		if ($simpan) {
			$this->session->set_flashdata('notif', alert_biasa('alasan di tolak berhasil disimpan !','success'));
			redirect('dosen_pa/detail_krs?'.$param,'refresh');
		}
	}

	public function hapus_riwayat_tolak($id)
	{
		$this->db->where('id', $id);
		$hapus = $this->db->delete('krs_ditolak');
		$this->session->set_flashdata('notif', alert_biasa('alasan di tolak berhasil dihapus !','success'));
		redirect('dosen_pa/detail_krs?'.param_get(),'refresh');

	}

	public function setujui()
	{
		$nim = $this->input->get('nim');
		$kode_semester = $this->input->get('kode_semester');
		$id_prodi = $this->input->get('id_prodi');

		$this->db->trans_begin();

		$this->db->where('nim', $nim);
		$this->db->where('kode_semester', $kode_semester);
		$this->db->update('temp_krs_pa', array('di_setujui'=>'y','update_at'=>get_waktu()));

		$this->db->where('nim', $nim);
		$this->db->where('kode_semester', $kode_semester);
		$this->db->update('krs', array('konfirmasi_pa'=>'y'));

		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', alert_notif('Gagal server','danger'));
            redirect('dosen_pa?id_prodi='.$id_prodi.'&kode_tahun='.$kode_semester,'refresh');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('message', alert_notif('Berhasil di setujui!','success'));
            redirect('dosen_pa?id_prodi='.$id_prodi.'&kode_tahun='.$kode_semester,'refresh');
        }
	}

	public function hapus()
	{
		$nim = $this->input->get('nim');
		$kode_semester = $this->input->get('kode_semester');
		$id_prodi = $this->input->get('id_prodi');

		$this->db->trans_begin();

		$this->db->where('nim', $nim);
		$this->db->where('kode_semester', $kode_semester);
		$this->db->delete('temp_krs_pa');

		$this->db->where('nim', $nim);
		$this->db->where('kode_semester', $kode_semester);
		$this->db->update('krs', array('konfirmasi_pa'=>'t'));

		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', alert_notif('Gagal server','danger'));
            redirect('dosen_pa?id_prodi='.$id_prodi.'&kode_tahun='.$kode_semester,'refresh');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('message', alert_notif('Berhasil di dihapus!','success'));
            redirect('dosen_pa?id_prodi='.$id_prodi.'&kode_tahun='.$kode_semester,'refresh');
        }
	}

	public function batalkan()
	{
		$nim = $this->input->get('nim');
		$kode_semester = $this->input->get('kode_semester');
		$id_prodi = $this->input->get('id_prodi');

		$this->db->trans_begin();

		$this->db->where('nim', $nim);
		$this->db->where('kode_semester', $kode_semester);
		$this->db->update('temp_krs_pa', array('di_setujui'=>'t','update_at'=>get_waktu()));

		$this->db->where('nim', $nim);
		$this->db->where('kode_semester', $kode_semester);
		$this->db->update('krs', array('konfirmasi_pa'=>'t'));

		if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', alert_notif('Gagal server','danger'));
            redirect('dosen_pa?id_prodi='.$id_prodi.'&kode_tahun='.$kode_semester,'refresh');
        }
        else
        {
            $this->db->trans_commit();
            $this->session->set_flashdata('message', alert_notif('Berhasil di dibatalkan!','success'));
            redirect('dosen_pa?id_prodi='.$id_prodi.'&kode_tahun='.$kode_semester,'refresh');
        }
	}

}
