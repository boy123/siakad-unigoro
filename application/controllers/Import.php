<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// if ($this->session->userdata('level') == 5) {
		// 	redirect('login','refresh');
		// }
	}

	public function tes()
	{
		$kode_tahun= tahun_akademik_aktif('kode_tahun');
        $id_tahun_akademik= tahun_akademik_aktif('id_tahun_akademik');

		$this->db->select('a.nim,a.id_kelas');
        $this->db->from('mahasiswa a');
        $this->db->join('registrasi b', 'a.nim = b.nim', 'inner');
        $this->db->where('b.id_tahun_akademik', $id_tahun_akademik);
        $this->db->where('b.kode_semester', $kode_tahun);
        $getdatamhs = $this->db->get();
        foreach ($getdatamhs->result() as $mhs) {
        	$semester = get_semester($mhs->nim);
        	$this->db->trans_begin();
        	$this->db->where('nim', $mhs->nim);
        	$this->db->where('id_tahun_akademik', $id_tahun_akademik);
        	$this->db->where('kode_semester', $kode_tahun);
        	$this->db->update('registrasi', array('semester'=>$semester));
        	$this->db->where('nim', $mhs->nim);
        	$this->db->update('mahasiswa', array('semester_aktif'=>$semester));
        	if ($this->db->trans_status() === FALSE)
			{
		        $this->db->trans_rollback();
		        log_data("gagal $mhs->nim");
			}
			else
			{
		        $this->db->trans_commit();
		        log_data("berhasil $mhs->nim");
			}
        }
	}

	public function update_mulai_semester()
	{
		log_r("no aktif fitur !!!");
		$no = 1;
		foreach ($this->db->get('mahasiswa')->result() as $rw) {
			$ang = substr($rw->nim, 0,2);
			$this->db->where('kode_tahun', '20'.$ang.'1');
			$kode_tahun = $this->db->get('tahun_akademik')->row()->kode_tahun;
			
			$this->db->where('nim', $rw->nim);
			$this->db->update('mahasiswa', array('mulai_semester'=>$kode_tahun));
			$berhasil = $no.') '.$rw->nim.' - '.$kode_tahun;
			log_data($berhasil);
			$no++;
		}
	}

	public function import_khs()
	{
		ini_set('memory_limit', '-1');
		if ($_FILES) {
			$return = array();
	        $this->load->library('upload'); // Load librari upload

	        $config['upload_path'] = './files/excel/';
	        $config['allowed_types'] = 'xlsx';
	        $config['max_size'] = '2048';
	        $config['overwrite'] = true;
	        $config['file_name'] = 'import_khs';

	        $this->upload->initialize($config); // Load konfigurasi uploadnya
	        if($this->upload->do_upload('file_excel')){ // Lakukan upload dan Cek jika proses upload berhasil
	            // Jika berhasil :
	            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
	        }else{
	            // Jika gagal :
	            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());

	            $this->session->set_flashdata('notif',alert_biasa($return['error'],'error'));
	            redirect('import/import_khs','refresh');
	        }

			// log_r($filename);

			include APPPATH.'third_party/PHPExcel/PHPExcel.php';

			$filename = "import_khs.xlsx";
						
			$excelreader = new PHPExcel_Reader_Excel2007();
			$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
			$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
			//skip untuk header
			unset($sheet[1]);
			$data['sheet'] = $sheet;
			$this->load->view('tes/import_khs', $data);
		} else {
			$this->load->view('tes/import_khs');
		}
	}

	public function aksi_import_khs()
	{
		ini_set('memory_limit', '-1');
		$id_prodi = $this->input->get('id_prodi');
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$filename = "import_khs.xlsx";
					
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		//skip untuk header
		unset($sheet[1]);

		$this->db->trans_begin();

		foreach ($sheet as $rw) {

			if ($rw['A'] != '' or !empty($rw['A'])) {
				$data = array(
					'nim' => $rw['A'],
					'kode_mk' => $rw['D'],
					'nama_mk' => $rw['E'],
					'kode_semester' => $rw['M'],
					'id_dosen' => $rw['F'] == '' ? '' : get_data('dosen','nidn',$rw['F'],'id_dosen'),
					'nama_dosen' => $rw['G'],
					'kelas' => $rw['H'],
					'sks' => $rw['I'],
					'angka' => $rw['J'],
					'huruf' => $rw['K'],
					'indeks' => str_replace(':', '.', $rw['L']),
					'id_prodi' => $id_prodi,
					'konfirmasi_pa' => 'y',
					'konfirmasi_nilai' => 'y',
					
				);
				$this->db->insert('krs', $data);
			}
			
		}

		if ($this->db->trans_status() === FALSE)
		{
	        $this->db->trans_rollback();
	        $this->session->set_flashdata('notif', alert_biasa('gagal server,silahkan ulangi','error'));
			redirect('import/import_khs','refresh');
		}
		else
		{
	        $this->db->trans_commit();
	        $this->session->set_flashdata('notif', alert_biasa('data khs mahasiswa berhasil diimport','success'));
			redirect('import/import_khs','refresh');
		}
	}

	public function import_kelas()
	{
		if ($_FILES) {
			$return = array();
	        $this->load->library('upload'); // Load librari upload

	        $config['upload_path'] = './files/excel/';
	        $config['allowed_types'] = 'xlsx';
	        $config['max_size'] = '2048';
	        $config['overwrite'] = true;
	        $config['file_name'] = 'import_kelas_mhs';

	        $this->upload->initialize($config); // Load konfigurasi uploadnya
	        if($this->upload->do_upload('file_excel')){ // Lakukan upload dan Cek jika proses upload berhasil
	            // Jika berhasil :
	            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
	        }else{
	            // Jika gagal :
	            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());

	            $this->session->set_flashdata('notif',alert_biasa($return['error'],'error'));
	            redirect('import/import_kelas','refresh');
	        }

			// log_r($filename);

			include APPPATH.'third_party/PHPExcel/PHPExcel.php';

			$filename = "import_kelas_mhs.xlsx";
						
			$excelreader = new PHPExcel_Reader_Excel2007();
			$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
			$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
			//skip untuk header
			unset($sheet[1]);
			unset($sheet[2]);
			$data['sheet'] = $sheet;
			$this->load->view('tes/import_kelas_mhs', $data);
		} else {
			$this->load->view('tes/import_kelas_mhs');
		}
	}

	public function import_akm()
	{
		ini_set('memory_limit', '-1');

		// include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		// $filename = "akm_mhs.xlsx";
					
		// $excelreader = new PHPExcel_Reader_Excel2007();
		// $loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
		// $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		// //skip untuk header
		// unset($sheet[1]);

		// foreach ($sheet as $rw) {
		// 	$nim = $rw['A'];
		// 	$id_mahasiswa = get_data('mahasiswa','nim',$nim,'id_mahasiswa');
  //       	$nama_mahasiswa = get_data('mahasiswa','nim',$nim,'nama');

		// 	$data_akm = array(
		// 		'id_mahasiswa' => $id_mahasiswa,
		// 		'nim' => $nim,
		// 		'nama' => $nama_mahasiswa,
		// 		'kode_semester' => $rw['C'],
		// 		'id_stat_mhs' => 'A'
		// 	);
		// 	// $this->db->insert('akm_mahasiswa', $data_akm);
		// }
		$db_old = $this->load->database('db_old', TRUE);
		$this->db->select('id_mahasiswa,nama,nim');
		foreach ($this->db->get('mahasiswa')->result() as $rw) {
			$db_old->where('tahun_akademik_id', 16);
			$db_old->where('nim', $rw->nim);
			$reg = $db_old->get('akademik_registrasi');
			if ($reg->num_rows() > 0) {
				$data_akm = array(
					'id_mahasiswa' => $rw->id_mahasiswa,
					'nim' => $rw->nim,
					'nama' => $rw->nama,
					'kode_semester' => '20201',
					'id_stat_mhs' => 'A'
				);
				$this->db->insert('akm_mahasiswa', $data_akm);
			} else {
				$data_akm = array(
					'id_mahasiswa' => $rw->id_mahasiswa,
					'nim' => $rw->nim,
					'nama' => $rw->nama,
					'kode_semester' => '20201',
					'id_stat_mhs' => 'N'
				);
				$this->db->insert('akm_mahasiswa', $data_akm);
			}
		}

		echo "berhasil";

		
		

	}

	public function import_indent()
	{
		ini_set('memory_limit', '-1');

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$filename = "indent1.xlsx";
					
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		//skip untuk header
		unset($sheet[1]);

		// log_r($sheet);

		foreach ($sheet as $rw) {
			$this->db->where('id_indent', $rw['A']);
			$cek_ind = $this->db->get('tr_po_dealer_indent');
			if ($cek_ind->num_rows() > 0) {
				$status = '0';
				$tgl_prospek = '';
				$tgl_deal = '';
				$tgl_cancel = '';
				$tgl_sales = '';
				if ($rw['K'] != '') {
					$status = "".$rw['K']."";
				} 

				if ($rw['M'] != '') {
					$tgl_prospek = $rw['M'];
				}

				if ($rw['N'] != '') {
					$tgl_deal = $rw['N'];
				}
				if ($rw['O'] != '') {
					$tgl_cancel = $rw['O'];
				}

				if ($rw['Q'] != '') {
					$tgl_sales = $rw['Q'];
				}


				$data = array(
					'date_prospek' => $tgl_prospek,
					'date_deal' => $tgl_deal != '' ? $tgl_deal : null,
					'date_cancel' => $tgl_cancel != '' ? $tgl_cancel : null,
					'date_sales' => $tgl_sales != '' ? $tgl_sales : null,
					'status_kirim' => $status
				);
				$this->db->where('id_indent', $rw['A']);
				$this->db->update('tr_po_dealer_indent', $data);
				log_data($this->db->last_query());
				// log_data($data);
			} else {
				log_data("no indent ".$rw['A']." ini tidak ditemukan !");
			}
		}
		

	}


	public function indent()
	{
		date_default_timezone_set('Asia/Jakarta');
		$data  = $this->db->query("
			SELECT
				tpdi.id_indent,
				'E20' AS kode_md,
				md.kode_dealer_ahm,
				tpdi.no_ktp,
				tpdi.id_tipe_kendaraan AS kode_varian,
				tpdi.id_warna AS kode_warna,
				'' AS kode_dummy_varian,
				'' AS kode_dummy_warna,
				ts.jenis_beli AS jenis_pembayaran,
				tpdi.date_konfirmasi AS tgl_unpaid,
				'' AS catatan,
				tpdi.status_kirim,
				tpdi.status,
				ts.status_spk AS status_spk,
				ts.updated_at AS tgl_cancel_spk,
				ts.no_spk AS id_spk,
				tpdi.id_reasons AS id_reasons,
				tpdi.id_dealer,
				tpdi.date_prospek,
				tpdi.date_deal,
				tpdi.date_cancel,
				tpdi.date_sales

			FROM
				tr_po_dealer_indent tpdi
				INNER JOIN ms_dealer md ON tpdi.id_dealer = md.id_dealer
				INNER JOIN tr_spk ts ON tpdi.id_spk = ts.no_spk 
				AND tpdi.send_ahm = '1' 
				AND tpdi.status_kirim NOT IN ('3', '4' )

			");

    	$content = "";
    	foreach ($data->result() as $rw) {

    		$flag_status = '1';
    		$jenis_pembayaran = '';
    		$tgl_paid = '';
    		$tgl_cancel = '';
    		$alasan_cancel_unpaid = '';
    		$alasan_cancel_paid = '';
    		$tgl_unpaid = '';
    		$kode_warna_final = '';
    		$eta_awal = '';
    		$eta_final = '';
    		$tgl_fulfillment = '';
    		$no_mesin = '';
    		$no_rangka = '';

    		

    		// jenis pembayaran
    		if ($rw->jenis_pembayaran == 'Cash') {
    			$jenis_pembayaran = '1';
    		} else {
    			$jenis_pembayaran = '2';
    		}

    		// tgl unpaid / prospek
    		if ($rw->tgl_unpaid != null) {
    			$tgl_unpaid = date('YmdHis',strtotime($rw->tgl_unpaid));

    			// cek tgl_unpaid jika di bawah tgl hari ini 
	    		if ($tgl_unpaid != '' and (strtotime($tgl_unpaid) < strtotime(date('YmdHis'))) ) {
					$tgl_unpaid = date('YmdHis');
				}
    		}

    		if ($rw->date_prospek != '') {
    			$tgl_unpaid = date('YmdHis', strtotime($rw->date_prospek));
    		}

    		

    		//tgl_paid
    		$this->db->where('no_spk', $rw->id_spk);
    		$this->db->limit(1);
    		$this->db->order_by('print_at', 'asc');
    		$cek_invoice = $this->db->get_where('tr_h1_dealer_invoice_receipt');
    		if ($cek_invoice->num_rows() > 0) {
    			$tgl_paid = date('YmdHis',strtotime($cek_invoice->row()->print_at));
    			//jika tgl paid lbih kecil dari tgl unpaid
    			if (strtotime($tgl_paid) < strtotime($tgl_unpaid)) {
    				$tgl_paid = date('YmdHis',strtotime($tgl_unpaid));
    			}
    		} else {
    			$tgl_paid = '';
    		}
    		if ($rw->date_deal != '') {
    			$tgl_paid = date('YmdHis', strtotime($rw->date_deal));
    		}

    		

    		// alasan cancel ketika paid
    		if ($tgl_paid != null && $rw->status_spk == 'canceled') {
    			$alasan_cancel_paid = $rw->id_reasons;
    		}

    		// alasan cancel ketika unpaid
    		if ($tgl_paid == null && $rw->status_spk == 'canceled') {
    			$alasan_cancel_unpaid = $rw->id_reasons;
    		}

    		// cek warna final
    		$this->db->select('id_warna');
    		$this->db->from('tr_spk ts');
    		$this->db->join('tr_sales_order tso', 'tso.no_spk = ts.no_spk', 'inner');
    		$this->db->where('tso.no_spk', $rw->id_spk);
    		$cek_warna_so = $this->db->get();
    		if ($cek_warna_so->num_rows() > 0) {
    			$kode_warna_final = $cek_warna_so->row()->id_warna;
    		}


    		//ambil tgl ETA awal
    		$tot_hari_eta_wal = $this->db->get_where('ms_master_lead_detail', array('id_tipe_kendaraan'=>$rw->kode_varian,'warna'=>$rw->kode_warna,'active'=>1));
    		if ($tot_hari_eta_wal->num_rows() > 0) {
    			$eta_awal = date('Ymd',strtotime('+'.$tot_hari_eta_wal->row()->total_lead_time.' days',strtotime($tgl_unpaid)));
    		}

    		// ambil ETA final
    		$cek_so = $this->db->get_where('tr_sales_order', array('no_spk'=>$rw->id_spk));
    		if ($cek_so->num_rows() > 0) {
    			//tgl_tgl_fulfillment
    			$tgl_fulfillment = date('Ymd',strtotime($cek_so->row()->tgl_cetak_invoice));
    			$no_mesin = $cek_so->row()->no_mesin;
    			$no_rangka = 'MH1'.$cek_so->row()->no_rangka;
    			$eta_final = date('Ymd',strtotime($cek_so->row()->tgl_cetak_invoice));
    			// cek ETA FINAL harus  ETA AWAL > ETA FINAL
    			if (strtotime($eta_awal) < strtotime($eta_final)) {
    				$eta_awal = $eta_final;
    			}
    		}

    		//flag_status
    		if ($tgl_unpaid != null) {
    			$flag_status = '1';
    		}
    		if ($tgl_paid != null) {
    			$flag_status = '2';
    		}
    		if ($rw->status_spk == 'canceled') {
    			$flag_status = '3';
    			$tgl_cancel = date('YmdHis',strtotime($rw->tgl_cancel_spk));
    		} 


    		// sebelumnya
    		// if ($tgl_paid != null and $kode_warna_final != '' and $eta_final != '') {
    		if ( $kode_warna_final != '' and $eta_final != '') {
    			$flag_status = '4';
    		}


    		// set cancel manual
    		if ($rw->status == 'canceled') {
    			$flag_status = '3';
    			$tgl_fulfillment = '';
    			$tgl_cancel = date('YmdHis',strtotime(date('Y-m-d H:i:s')));
    			// alasan cancel ketika paid
	    		if ($tgl_paid != null) {
	    			$alasan_cancel_paid = $rw->id_reasons;
	    		}

	    		// alasan cancel ketika unpaid
	    		if ($tgl_paid == null) {
	    			$alasan_cancel_unpaid = $rw->id_reasons;
	    		}
    		} 

    		//kirim jika flag status di atas status kmren
    		if ($rw->status_kirim < $flag_status or $rw->status_kirim == '1') {
    			

    			// jika status kirim adalah deal (2), maka tgl boleh di isi hanya tgl paid, tgl fullfilment dan tgl eta final wajib dikosongkan
    			if ($flag_status == '2') {
    				$tgl_fulfillment = '';
    				$eta_final = '';
    			}

    			// cek tgl cancel jika di bawah tgl hari ini 
    			if ($flag_status == '3') {
    				if ($tgl_cancel != '' and (strtotime($tgl_cancel) < strtotime(date('YmdHis'))) ) {
						$tgl_cancel = date('YmdHis');
					}  
    			}

    			// cek tgl paid/deal jika di bawah tgl hari ini 
    			if ($flag_status == '2') {
    				if ($tgl_paid != '' and (strtotime($tgl_paid) < strtotime(date('YmdHis'))) ) {
						$tgl_paid = date('YmdHis');
					}  
    			}
	    		  		

				if ($flag_status == '4') {
				 	// cek tgl fulfil jika di bawah tgl hari ini 
		    		if ($tgl_fulfillment != '' and (strtotime($tgl_fulfillment) < strtotime(date('YmdHis'))) ) {
						$tgl_fulfillment = date('YmdHis');
					}  
					// cek tgl eta final jika di bawah tgl hari ini 
		    		if ($eta_final != '' and (strtotime($eta_final) < strtotime(date('Ymd'))) ) {
						$eta_final = date('Ymd');
					}
				 } 

				// cek tgl eta awal jika di bawah tgl hari ini 
	    		if ($eta_awal != '' and (strtotime($eta_awal) < strtotime(date('Ymd'))) ) {
					$eta_awal = date('Ymd');
				}

				// cek tgl eta final jika di bawah tgl hari ini 
	    		if ($eta_final != '' and (strtotime($eta_final) < strtotime(date('Ymd'))) ) {
					$eta_final = date('Ymd');
				}

				// jika status kirim sebelum nya masih 1 belum menjadi 2
    			// dan flag berstatus 3 (cancel) maka status flag ubah jadi 2
    			if ($rw->status_kirim == '1' and $flag_status == '3' and $tgl_paid != null) {
    				$flag_status = '2';
    				$tgl_cancel = '';
    				$tgl_fulfillment = '';
    				$no_rangka= '';
    				$no_mesin = '';
    				$alasan_cancel_unpaid = '';
    				$alasan_cancel_paid = '';
    				$kode_warna_final = '';
    				$eta_final = '';
    			}


				// jika status kirim sebelum nya masih 1 belum menjadi 2
    			// dan flag berstatus 4 (sales) maka status flag ubah jadi 2
    			if ($rw->status_kirim == '1' and $flag_status == '4') {
    				$flag_status = '2';
    				$tgl_cancel = '';
    				$tgl_fulfillment = '';
    				$no_rangka= '';
    				$no_mesin = '';
    				$alasan_cancel_unpaid = '';
    				$alasan_cancel_paid = '';
    				$kode_warna_final = '';
    				$eta_final = '';
    			}

    			if ($rw->status_kirim == '0' and $flag_status == '4') {
    				$flag_status = '1';
    				$tgl_cancel = '';
    				$tgl_fulfillment = '';
    				$no_rangka= '';
    				$no_mesin = '';
    				$alasan_cancel_unpaid = '';
    				$alasan_cancel_paid = '';
    				$kode_warna_final = '';
    				$eta_final = '';
    			}

				

    			$content .= "$rw->id_indent;$rw->kode_md;$rw->kode_dealer_ahm;$rw->no_ktp;$rw->kode_varian;$rw->kode_warna;$rw->kode_dummy_varian;$rw->kode_dummy_warna;$flag_status;$jenis_pembayaran;$tgl_unpaid;$tgl_paid;$tgl_cancel;$tgl_fulfillment;$no_rangka;$no_mesin;$alasan_cancel_unpaid;$alasan_cancel_paid;$kode_warna_final;$eta_awal;$eta_final;$rw->catatan; \r\n";
				// update status
				$update_po_ind = array(
	    			'date_prospek' => $tgl_unpaid != '' ? date('Y-m-d H:i:s', strtotime($tgl_unpaid)) : null,
	    			'date_deal'=>$tgl_paid != '' ? date('Y-m-d H:i:s', strtotime($tgl_paid)) : null,
	    			'date_cancel' => $tgl_cancel != '' ? date('Y-m-d H:i:s', strtotime($tgl_cancel)) : null,
	    			'date_sales' => $eta_final != '' ? date('Y-m-d H:i:s', strtotime($eta_final)) : null,
	    			'eta_awal' => $eta_awal != '' ? date('Y-m-d', strtotime($eta_awal)) : null,
	    			'eta_final' => $eta_final != '' ? date('Y-m-d', strtotime($eta_final)) : null,
	    			'status_kirim'=>$flag_status);

				// log_data($rw->id_indent);
				// log_data($update_po_ind);
				$this->db->where('id_indent', $rw->id_indent);
	    		$this->db->update('tr_po_dealer_indent', $update_po_ind);
    		}
			
		}
		$name_file = "AHM-E20-".date('ymd')."-".date('ymdhis').".UIND";

		// echo $content;

		// jika mau disimpan

		// $fp = fopen($_SERVER['DOCUMENT_ROOT'].'/downloads/uind/' . $name_file,"wb");
		// fwrite($fp,$content);
		// fclose($fp);

		$this->load->helper('download');
		// auto download
		force_download($name_file, $content);
	}

	public function aksi_import_kelas_mhs()
	{
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$filename = "import_kelas_mhs.xlsx";
					
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		//skip untuk header
		unset($sheet[1]);

		$this->db->trans_begin();

		foreach ($sheet as $rw) {
			$cek_nim = get_data('mahasiswa','nim',$rw['A'],'nim');
			$id_kelas = get_data('kelas','kelas',$rw['B'],'id_kelas');
			if ($cek_nim != '') {
				$this->db->where('nim', $rw['A']);
				$this->db->update('mahasiswa', array('id_kelas'=>$id_kelas));
			} else {

			}
			
		}

		if ($this->db->trans_status() === FALSE)
		{
	        $this->db->trans_rollback();
	        $this->session->set_flashdata('notif', alert_biasa('gagal server,silahkan ulangi','error'));
			redirect('import/import_kelas','refresh');
		}
		else
		{
	        $this->db->trans_commit();
	        $this->session->set_flashdata('notif', alert_biasa('data kelas mahasiswa berhasil diimport','success'));
			redirect('import/import_kelas','refresh');
		}
	}
	
	public function import_mk_kurikulum()
	{
		$id_prodi = $this->input->get('id_prodi');
		$id_kurikulum = $this->input->get('id_kurikulum');
		if ($id_prodi == '' or $id_kurikulum =='') {
			$this->session->set_flashdata('notif', alert_biasa('silahkan pilih prodi dan kurikulum terlebih dahulu','error'));
			redirect('matakuliah','refresh');
		}

		// Fungsi untuk melakukan proses upload file
        $return = array();
        $this->load->library('upload'); // Load librari upload

        $config['upload_path'] = './files/excel/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size'] = '2048';
        $config['overwrite'] = true;
        $config['file_name'] = 'import_mk';

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file_excel')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());

            $this->session->set_flashdata('notif',alert_biasa($return['error'],'error'));
            redirect('matakuliah?id_prodi='.$id_prodi.'&id_kurikulum='.$id_kurikulum,'refresh');
        }

		// log_r($filename);

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$filename = "import_mk.xlsx";
					
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		//skip untuk header
		unset($sheet[1]);

		$this->db->trans_begin();

		foreach ($sheet as $rw) {
			$data = array(
				'kode_mk' => $rw['A'],
				'nama_mk' => $rw['B'],
				'jenis_mk' => $rw['C'],
				'sks_tm' => $rw['D'],
				'sks_prak' => $rw['E'],
				'sks_prak_la' => $rw['F'],
				'sks_simulasi' => $rw['G'],
				'sks_total' => $rw['D'] + $rw['E'] + $rw['F'] + $rw['G'],
				'metode_pembelajaran' => $rw['H'],
				'tgl_mulai_efektif' => $rw['I'],
				'tgl_akhir_efektif' => $rw['J'],
				'semester' => $rw['K'],
				'id_prodi' => $id_prodi,
				'id_kurikulum' => $id_kurikulum,
				
			);
			$this->db->insert('matakuliah', $data);
		}

		if ($this->db->trans_status() === FALSE)
		{
	        $this->db->trans_rollback();
	        $this->session->set_flashdata('notif', alert_biasa('gagal server,silahkan ulangi','error'));
			redirect('matakuliah?id_prodi='.$id_prodi.'&id_kurikulum='.$id_kurikulum,'refresh');
		}
		else
		{
	        $this->db->trans_commit();
	        $this->session->set_flashdata('notif', alert_biasa('data matakuliah berhasil diimport','success'));
			redirect('matakuliah?id_prodi='.$id_prodi.'&id_kurikulum='.$id_kurikulum,'refresh');
		}

	}


	public function import_mk()
	{
		$id_prodi = $this->input->get('id_prodi');
		if ($id_prodi == '' ) {
			$this->session->set_flashdata('notif', alert_biasa('silahkan pilih prodi terlebih dahulu','error'));
			redirect('master_matakuliah','refresh');
		}

		// Fungsi untuk melakukan proses upload file
        $return = array();
        $this->load->library('upload'); // Load librari upload

        $config['upload_path'] = './files/excel/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size'] = '2048';
        $config['overwrite'] = true;
        $config['file_name'] = 'import_mk';

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file_excel')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());

            $this->session->set_flashdata('notif',alert_biasa($return['error'],'error'));
            redirect('matakuliah','refresh');
        }

		// log_r($filename);

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$filename = "import_mk.xlsx";
					
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		//skip untuk header
		unset($sheet[1]);

		$this->db->trans_begin();

		foreach ($sheet as $rw) {
			$data = array(
				'kode_mk' => $rw['A'],
				'nama_mk' => $rw['B'],
				'jenis_mk' => $rw['C'],
				'sks_tm' => $rw['D'],
				'sks_prak' => $rw['E'],
				'sks_prak_la' => $rw['F'],
				'sks_simulasi' => $rw['G'],
				'sks_total' => $rw['D'] + $rw['E'] + $rw['F'] + $rw['G'],
				'metode_pembelajaran' => $rw['H'],
				'tgl_mulai_efektif' => $rw['I'],
				'tgl_akhir_efektif' => $rw['J'],
				'semester' => $rw['K'],
				'id_prodi' => $id_prodi,
				
			);
			$this->db->insert('master_matakuliah', $data);
		}

		if ($this->db->trans_status() === FALSE)
		{
	        $this->db->trans_rollback();
	        $this->session->set_flashdata('notif', alert_biasa('gagal server,silahkan ulangi','error'));
			redirect('master_matakuliah?id_prodi='.$id_prodi,'refresh');
		}
		else
		{
	        $this->db->trans_commit();
	        $this->session->set_flashdata('notif', alert_biasa('data matakuliah berhasil diimport','success'));
			redirect('master_matakuliah?id_prodi='.$id_prodi,'refresh');
		}

	}


	public function import_dosen()
	{
		$id_prodi = $this->input->post('id_prodi');
		if ($id_prodi == '' ) {
			$this->session->set_flashdata('notif', alert_biasa('silahkan pilih prodi terlebih dahulu','error'));
			redirect('dosen','refresh');
		}

		// Fungsi untuk melakukan proses upload file
        $return = array();
        $this->load->library('upload'); // Load librari upload

        $config['upload_path'] = './files/excel/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size'] = '2048';
        $config['overwrite'] = true;
        $config['file_name'] = 'import_dosen';

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file_excel')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());

            $this->session->set_flashdata('notif',alert_biasa($return['error'],'error'));
            redirect('dosen','refresh');
        }

		// log_r($filename);

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$filename = "import_dosen.xlsx";
					
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		//skip untuk header

		unset($sheet[1]);

		$this->db->trans_begin();

		foreach ($sheet as $rw) {
			$data = array(
				'nidn' => trim($rw['A']),
				'nip' => trim($rw['B']),
				'no_pegawai' => trim($rw['C']),
				'nama' => $rw['D'],
				'jenis_kelamin' => $rw['E'],
				'tempat_lahir' => $rw['F'],
				'tanggal_lahir' => $rw['G'],
				'id_prodi' => $id_prodi,
				
			);
			$this->db->insert('dosen', $data);
			$this->db->insert('users', array(
                'nama' => $rw['D'],
                'username' => $retVal = (trim($rw['A']) != '') ? trim($rw['A']) : trim($rw['C']),
                'password' => password_hash('123456', PASSWORD_DEFAULT),
                'level' => '4',
                'keterangan' => $this->db->insert_id(),
                'id_prodi' => $id_prodi,
            ));
		}

		if ($this->db->trans_status() === FALSE)
		{
	        $this->db->trans_rollback();
	        $this->session->set_flashdata('notif', alert_biasa('gagal server,silahkan ulangi','error'));
			redirect('dosen?id_prodi='.$id_prodi,'refresh');
		}
		else
		{
	        $this->db->trans_commit();
	        $this->session->set_flashdata('notif', alert_biasa('data dosen berhasil diimport','success'));
			redirect('dosen?id_prodi='.$id_prodi,'refresh');
		}

	}

	public function import_mahasiswa()
	{
		// log_r($_FILES);
		$id_prodi = $this->input->post('id_prodi');
		$id_tahun_angkatan = $this->input->post('id_tahun_angkatan');
		$tahun_akt = get_data('tahun_angkatan','id_tahun_angkatan',$id_tahun_angkatan,'tahun_angkatan');
		if ($id_prodi == '' ) {
			$this->session->set_flashdata('notif', alert_biasa('silahkan pilih prodi terlebih dahulu','error'));
			redirect('mahasiswa','refresh');
		}

		// Fungsi untuk melakukan proses upload file
        $return = array();
        $this->load->library('upload'); // Load librari upload

        $config['upload_path'] = './files/excel/';
        $config['allowed_types'] = 'xlsx|';
        $config['max_size'] = '2048';
        $config['overwrite'] = true;
        $config['file_name'] = 'import_mahasiswa';

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file_excel')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());

            $this->session->set_flashdata('notif',alert_biasa($return['error'],'error'));
            redirect('mahasiswa?id_prodi='.$id_prodi,'refresh');
        }

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$filename = "import_mahasiswa.xlsx";
					
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		//skip untuk header

		unset($sheet[1]);

		$raw = array();

		$this->db->trans_begin();

		foreach ($sheet as $rw) {
			if (empty($rw['A'])) {
				// code...
			} else {
				$id_prodi = get_data('prodi','kode_prodi',$rw['AT'],'id_prodi');
				$data = array(
					'nim' => $rw['A'],
					'nama' => $rw['B'],
					'tempat_lahir' => $rw['C'],
					'tanggal_lahir' => $rw['D'],
					'jenis_kelamin' => $rw['E'],
					'nik' => $rw['F'],
					'agama' => $rw['G'],
					'nisn' => $rw['H'],
					'jalur_pendaftaran' => $rw['I'],
					'npwp' => $rw['J'],
					'kewarganegaraan' => $rw['K'],
					'jenis_pendaftaran' => $rw['L'],
					'tanggal_masuk_kuliah' => $rw['M'],
					'mulai_semester' => $tahun_akt.'1',
					'jalan' => $rw['O'],
					'rt' => $rw['P'],
					'rw' => $rw['Q'],
					'dusun' => $rw['R'],
					'kelurahan' => $rw['S'],
					'kecamatan' => $rw['T'],
					'kode_pos' => $rw['U'],
					'jenis_tinggal' => $rw['V'],
					'alat_transportasi' => $rw['W'],
					'telp_rumah' => $rw['X'],
					'no_hp' => $rw['Y'],
					'email' => $rw['Z'],
					'terima_kps' => $rw['AA'],
					'no_kps' => $rw['AB'],
					'nik_ayah' => $rw['AC'],
					'nama_ayah' => $rw['AD'],
					'tanggal_lahir_ayah' => $rw['AE'],
					'pendidikan_ayah' => $rw['AF'],
					'pekerjaan_ayah' => $rw['AG'],
					'penghasilan_ayah' => $rw['AH'],
					'nik_ibu' => $rw['AI'],
					'nama_ibu' => $rw['AJ'],
					'tanggal_lahir_ibu' => $rw['AK'],
					'pendidikan_ibu' => $rw['AL'],
					'pekerjaan_ibu' => $rw['AM'],
					'penghasilan_ibu' => $rw['AN'],
					'nama_wali' => $rw['AO'],
					'tanggal_lahir_wali' => $rw['AP'],
					'pendidikan_wali' => $rw['AQ'],
					'pekerjaan_wali' => $rw['AR'],
					'penghasilan_wali' => $rw['AS'],
					'id_prodi' => $id_prodi,
					'id_tahun_angkatan'=>$id_tahun_angkatan,
					'jenis_pembiayaan'=> $rw['AU'],
					'jumlah_biaya_masuk'=> $rw['AV'],
					'sks_diakui'=> $rw['AW'],
					'asal_pt'=> $rw['AX'],
					'asal_prodi'=> $rw['AY'],
				);

				// array_push($raw, $data);

				$this->db->insert('mahasiswa', $data);
				$this->db->insert('users', array(
	                'nama' => $rw['B'],
	                'username' => $rw['A'],
	                'password' => password_hash('123456', PASSWORD_DEFAULT),
	                'level' => '5',
	                'keterangan' => $this->db->insert_id(),
	                'id_prodi' => $id_prodi,
	            ));
			}
			
		}

		// log_r($raw);

		if ($this->db->trans_status() === FALSE)
		{
	        $this->db->trans_rollback();
	        $this->session->set_flashdata('notif', alert_biasa('gagal server,silahkan ulangi','error'));
			redirect('mahasiswa?id_prodi='.$id_prodi,'refresh');
		}
		else
		{
	        $this->db->trans_commit();
	        $this->session->set_flashdata('notif', alert_biasa('data mahasiswa berhasil diimport','success'));
			redirect('mahasiswa?id_prodi='.$id_prodi,'refresh');
		}
	}

	public function import_mahasiswa_custom()
	{
		// log_r($_FILES);
		// $id_prodi = $this->input->post('id_prodi');
		// $id_tahun_angkatan = $this->input->post('id_tahun_angkatan');
		// $tahun_akt = get_data('tahun_angkatan','id_tahun_angkatan',$id_tahun_angkatan,'tahun_angkatan');
		// if ($id_prodi == '' ) {
		// 	$this->session->set_flashdata('notif', alert_biasa('silahkan pilih prodi terlebih dahulu','error'));
		// 	redirect('mahasiswa','refresh');
		// }

		// Fungsi untuk melakukan proses upload file
        // $return = array();
        // $this->load->library('upload'); // Load librari upload

        // $config['upload_path'] = './files/excel/';
        // $config['allowed_types'] = 'xlsx|';
        // $config['max_size'] = '2048';
        // $config['overwrite'] = true;
        // $config['file_name'] = 'import_mahasiswa';

        // $this->upload->initialize($config); // Load konfigurasi uploadnya
        // if($this->upload->do_upload('file_excel')){ // Lakukan upload dan Cek jika proses upload berhasil
        //     // Jika berhasil :
        //     $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
        // }else{
        //     // Jika gagal :
        //     $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());

        //     $this->session->set_flashdata('notif',alert_biasa($return['error'],'error'));
        //     redirect('mahasiswa?id_prodi='.$id_prodi,'refresh');
        // }

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$filename = "import_mahasiswa_custom.xlsx";
					
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		//skip untuk header

		unset($sheet[1]);

		$raw = array();

		$this->db->trans_begin();

		foreach ($sheet as $rw) {
			if (empty($rw['A'])) {
				// code...
			} else {
				$tahun_akt = '20'.substr($rw['A'], 0, 2);
				$id_tahun_angkatan = get_data('tahun_angkatan','tahun_angkatan',$tahun_akt,'id_tahun_angkatan');
				$id_prodi = get_data('prodi','kode_prodi',$rw['AT'],'id_prodi');
				$data = array(
					'nim' => $rw['A'],
					'nama' => $rw['B'],
					'tempat_lahir' => $rw['C'],
					'tanggal_lahir' => $rw['D'],
					'jenis_kelamin' => $rw['E'],
					'nik' => $rw['F'],
					'agama' => $rw['G'],
					'nisn' => $rw['H'],
					'jalur_pendaftaran' => $rw['I'],
					'npwp' => $rw['J'],
					'kewarganegaraan' => $rw['K'],
					'jenis_pendaftaran' => $rw['L'],
					'tanggal_masuk_kuliah' => $rw['M'],
					'mulai_semester' => $tahun_akt.'1',
					'jalan' => $rw['O'],
					'rt' => $rw['P'],
					'rw' => $rw['Q'],
					'dusun' => $rw['R'],
					'kelurahan' => $rw['S'],
					'kecamatan' => $rw['T'],
					'kode_pos' => $rw['U'],
					'jenis_tinggal' => $rw['V'],
					'alat_transportasi' => $rw['W'],
					'telp_rumah' => $rw['X'],
					'no_hp' => $rw['Y'],
					'email' => $rw['Z'],
					'terima_kps' => $rw['AA'],
					'no_kps' => $rw['AB'],
					'nik_ayah' => $rw['AC'],
					'nama_ayah' => $rw['AD'],
					'tanggal_lahir_ayah' => $rw['AE'],
					'pendidikan_ayah' => $rw['AF'],
					'pekerjaan_ayah' => $rw['AG'],
					'penghasilan_ayah' => $rw['AH'],
					'nik_ibu' => $rw['AI'],
					'nama_ibu' => $rw['AJ'],
					'tanggal_lahir_ibu' => $rw['AK'],
					'pendidikan_ibu' => $rw['AL'],
					'pekerjaan_ibu' => $rw['AM'],
					'penghasilan_ibu' => $rw['AN'],
					'nama_wali' => $rw['AO'],
					'tanggal_lahir_wali' => $rw['AP'],
					'pendidikan_wali' => $rw['AQ'],
					'pekerjaan_wali' => $rw['AR'],
					'penghasilan_wali' => $rw['AS'],
					'id_prodi' => $id_prodi,
					'id_tahun_angkatan'=>$id_tahun_angkatan,
					'jenis_pembiayaan'=> $rw['AU'],
					'jumlah_biaya_masuk'=> $rw['AV'],
					'sks_diakui'=> $rw['AW'],
					'asal_pt'=> $rw['AX'],
					'asal_prodi'=> $rw['AY'],
				);

				// array_push($raw, $data);

				$this->db->insert('mahasiswa', $data);
				$this->db->insert('users', array(
	                'nama' => $rw['B'],
	                'username' => $rw['A'],
	                'password' => password_hash('123456', PASSWORD_DEFAULT),
	                'level' => '5',
	                'keterangan' => $this->db->insert_id(),
	                'id_prodi' => $id_prodi,
	            ));
			}
			
		}

		// log_r($raw);

		if ($this->db->trans_status() === FALSE)
		{
	        $this->db->trans_rollback();
	        $this->session->set_flashdata('notif', alert_biasa('gagal server,silahkan ulangi','error'));
			redirect('mahasiswa?id_prodi='.$id_prodi,'refresh');
		}
		else
		{
	        $this->db->trans_commit();
	        $this->session->set_flashdata('notif', alert_biasa('data mahasiswa berhasil diimport','success'));
			redirect('mahasiswa?id_prodi='.$id_prodi,'refresh');
		}
	}

	public function import_krs_db()
	{
		// log_r('dimatikan');
		$db_old = $this->load->database('db_old', TRUE);
		// $db_old->where('tahun_akademik_id', 13);
		// $db_old->order_by('nim', 'desc');
		// foreach ($db_old->get('nilai')->result() as $rw) {
		// 	$kode_prodi = $db_old->get_where('akademik_konsentrasi', array('konsentrasi_id' => $rw->konsentrasi_id))->row()->kode_prodi;
		// 	$id_prodi = get_data('prodi','kode_prodi',$kode_prodi,'id_prodi');
		// 	$data = array(
		// 		'nim' => $rw->nim,
		// 		'kode_mk' => $rw->kode_makul,
		// 		'nama_mk' => $rw->nama_makul,
		// 		'kode_semester' => '20192',
		// 		'id_dosen' => get_data('dosen','nidn',$rw->nidn,'id_dosen'),
		// 		'nama_dosen' => $rw->nama_dosen,
		// 		'kelas' => '',
		// 		'sks' => $rw->sks,
		// 		'angka' => $rw->nilai,
		// 		'huruf' => $rw->grade,
		// 		'indeks' => $rw->mutu,
		// 		'id_prodi' => $id_prodi,
		// 		'konfirmasi_pa' => 'y',
		// 		'konfirmasi_nilai' => 'y',
				
		// 	);
		// 	$this->db->insert('krs', $data);
		// }
		$this->db->select('nim,kode_mk');
		$this->db->where('kode_semester', '20201');
		foreach ($this->db->get('krs')->result() as $rw) {
			$db_old->where('nim', $rw->nim);
			$db_old->where('kode_makul', $rw->kode_mk);
			$db_old->where('tahun_akademik_id', 16);
			$id_ruang = $db_old->get('nilai')->row()->ruangan_id;
			$nama_ruang = $db_old->get_where('app_ruangan', array('ruangan_id'=>$id_ruang))->row()->nama_ruangan;

			// echo $rw->nim.' | '.$rw->kode_mk.' | '.$nama_ruang.'<br>';
			$this->db->where('nim', $rw->nim);
			$this->db->where('kode_semester', '20201');
			$this->db->where('kode_mk', $rw->kode_mk);
			$this->db->update('krs', array('kelas'=>$nama_ruang));

		}
		echo "berhasil";
	}

	public function import_krs($aksi='')
	{
		if ($aksi != 'yaya7778') {
			echo "salah perintah";
			exit();
		}
		
		$kode_semester = '20201';
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$filename = "import_krs_".$kode_semester."_ti.xlsx";
					
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		//skip untuk header

		unset($sheet[1]);

		$this->db->trans_begin();

		foreach ($sheet as $rw) {
			$data = array(
				'nim' => $rw['A'],
				'kode_mk' => $rw['D'],
				'nama_mk' => $rw['E'],
				'kode_semester' => $kode_semester,
				'id_dosen' => get_data('dosen','nidn',$rw['F'],'id_dosen'),
				'nama_dosen' => $rw['G'],
				'kelas' => $rw['H'],
				'sks' => $rw['I'],
				'angka' => $rw['J'],
				'huruf' => $rw['K'],
				'indeks' => $rw['L'],
				'konfirmasi_pa' => 'y',
				'konfirmasi_nilai' => 'y',
				
			);
			// log_r($data);
			$this->db->insert('krs', $data);
			$id_krs = $this->db->insert_id();
			$this->db->insert('absen', array(
				'nim' => $rw['A'],
				'id_krs' => $id_krs,
			));
		}

		if ($this->db->trans_status() === FALSE)
		{
	        $this->db->trans_rollback();
	        echo "gagal server";
		}
		else
		{
	        $this->db->trans_commit();
	        echo "krs berhasil disimpan $kode_semester ti";
		}

	}

	public function import_dosen_pa()
	{
		if ($_FILES) {
			$return = array();
	        $this->load->library('upload'); // Load librari upload

	        $config['upload_path'] = './files/excel/';
	        $config['allowed_types'] = 'xlsx';
	        $config['max_size'] = '2048';
	        $config['overwrite'] = true;
	        $config['file_name'] = 'import_dosen_pa';

	        $this->upload->initialize($config); // Load konfigurasi uploadnya
	        if($this->upload->do_upload('file_excel')){ // Lakukan upload dan Cek jika proses upload berhasil
	            // Jika berhasil :
	            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
	        }else{
	            // Jika gagal :
	            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());

	            $this->session->set_flashdata('notif',alert_biasa($return['error'],'error'));
	            redirect('mahasiswa','refresh');
	        }

			// log_r($filename);

			include APPPATH.'third_party/PHPExcel/PHPExcel.php';

			$filename = "import_dosen_pa.xlsx";
						
			$excelreader = new PHPExcel_Reader_Excel2007();
			$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
			$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
			//skip untuk header
			//skip untuk header
			unset($sheet[1]);

			$this->db->trans_begin();

			foreach ($sheet as $rw) {
				$cek_nim = get_data('mahasiswa','nim',$rw['A'],'nim');
				$n_dosen = $rw['C'];
				$dosen_sql = $this->db->query("
					SELECT id_dosen FROM dosen WHERE nidn='$n_dosen' or no_pegawai='$n_dosen' 
					");
				$id_dosen = $dosen_sql->row()->id_dosen;
				if ($cek_nim != '') {
					$this->db->where('nim', $rw['A']);
					$this->db->update('mahasiswa', array('dosen_pa'=>$id_dosen));
				} else {

				}
				
			}

			if ($this->db->trans_status() === FALSE)
			{
		        $this->db->trans_rollback();
		        $this->session->set_flashdata('notif', alert_biasa('gagal server,silahkan ulangi','error'));
				redirect('mahasiswa','refresh');
			}
			else
			{
		        $this->db->trans_commit();
		        $this->session->set_flashdata('notif', alert_biasa('data dosen pa berhasil diimport','success'));
				redirect('mahasiswa','refresh');
			}
		} else {
			redirect('mahasiswa','refresh');
		}
	}






}
