<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	
	public function index()
	{
		include_once APPPATH . "../vendor/autoload.php";

		$google_client = new Google_Client();
		$redirect_url = base_url().'login.html';
		$google_client->setClientId('225971548269-2s1dr6flgal844envebp5lsubkaeoq94.apps.googleusercontent.com'); //masukkan ClientID anda 
		$google_client->setClientSecret('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'); //masukkan Client Secret Key anda
		$google_client->setRedirectUri($redirect_url); //Masukkan Redirect Uri anda
		$google_client->addScope('email');
		$google_client->addScope('profile');

		if(isset($_GET["code"]))
		{
			$token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);
			if(!isset($token["error"])) {
				$google_client->setAccessToken($token['access_token']);
				$this->session->set_userdata('access_token', $token['access_token']);
				$google_service = new Google_Service_Oauth2($google_client);
				$data_google = $google_service->userinfo->get();
				$current_datetime = date('Y-m-d H:i:s');
				
				$email = $data_google['email'];
				$this->db->where('email', $email);
				$cek = $this->db->get('users');
				if ($cek->num_rows() == 0) {
					$this->session->set_flashdata('message', alert_biasa('Gagal Login!\n email tidak ditemukan','warning'));
					redirect('login','refresh');
				} else {
					$users = $cek->row();
					// jika berhasil login
					$sess_data['id_user'] = $users->id_user;
					$sess_data['nama'] = $users->nama;
					$sess_data['username'] = $users->username;
					$sess_data['foto'] = $users->foto;
					$sess_data['level'] = $users->level;
					$sess_data['keterangan'] = $users->keterangan;
					$this->session->set_userdata($sess_data);
					$this->rbac->set_access_in_session();

					// update last login
					$this->db->where('id_user', $users->id_user);
					$up_last = $this->db->update('users', array('last_login'=>get_waktu()));

					if ($up_last) {
						redirect('app','refresh');
					} else {
						$this->session->set_flashdata('message', alert_biasa('Gagal Login!\n ada kesalahan server','warning'));
						redirect('login','refresh');
					}

				}

			}									
		}
		if ($this->session->userdata('level') != '' && $this->session->userdata('id_user') != '') {
			redirect('app/index','refresh');
		}
		$data['judul_page'] = 'Login Siakad';
		$data['google_link'] = $google_client->createAuthUrl();
		$this->load->view('login_new',$data);
	}

	public function auth()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$a = $this->input->post('a');
		$b = $this->input->post('b');
		$hasil = $this->input->post('hasil');
		$result = $a + $b;
		if ($_POST) {
			if ($hasil != $result) {
				$this->session->set_flashdata('message', alert_biasa('Captcha yang anda masukkan salah !','warning'));
				redirect('login','refresh');
			} else {
				$this->db->where('username', $username);
				$cek = $this->db->get('users');
				if ($cek->num_rows() == 0) {
					$this->session->set_flashdata('message', alert_biasa('Gagal Login!\n username tidak ditemukan','warning'));
					redirect('login','refresh');
				} else {
					$users = $cek->row();
					if (password_verify($password, $users->password)) {
						// jika berhasil login
						$sess_data['id_user'] = $users->id_user;
						$sess_data['nama'] = $users->nama;
						$sess_data['username'] = $users->username;
						$sess_data['foto'] = $users->foto;
						$sess_data['level'] = $users->level;
						$sess_data['keterangan'] = $users->keterangan;
						$this->session->set_userdata($sess_data);
						$this->rbac->set_access_in_session();

						// update last login
						$this->db->where('id_user', $users->id_user);
						$up_last = $this->db->update('users', array('last_login'=>get_waktu()));

						if ($up_last) {
							redirect('app','refresh');
						} else {
							$this->session->set_flashdata('message', alert_biasa('Gagal Login!\n ada kesalahan server','warning'));
							redirect('login','refresh');
						}

					} else {
						$this->session->set_flashdata('message', alert_biasa('Gagal Login!\n password kamu salah','warning'));
						redirect('login','refresh');
					}
				}
			}
		}
	}

	public function auth_pass()
	{
		$username = $this->input->get('username');
		$this->db->where('username', $username);
		$cek = $this->db->get('users');
		if ($cek->num_rows() == 0) {
			$this->session->set_flashdata('message', alert_biasa('Gagal Login!\n username tidak ditemukan','warning'));
			redirect('login','refresh');
		} else {
			$users = $cek->row();
			// jika berhasil login
			$sess_data['id_user'] = $users->id_user;
			$sess_data['nama'] = $users->nama;
			$sess_data['username'] = $users->username;
			$sess_data['foto'] = $users->foto;
			$sess_data['level'] = $users->level;
			$sess_data['keterangan'] = $users->keterangan;
			$this->session->set_userdata($sess_data);
			$this->rbac->set_access_in_session();

			// update last login
			$this->db->where('id_user', $users->id_user);
			$up_last = $this->db->update('users', array('last_login'=>get_waktu()));

			if ($up_last) {
				redirect('app','refresh');
			} else {
				$this->session->set_flashdata('message', alert_biasa('Gagal Login!\n ada kesalahan server','warning'));
				redirect('login','refresh');
			}
		}
	}

	function logout()
	{

		$this->session->unset_userdata('id_user');
		$this->session->unset_userdata('nama');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('login','refresh');
	}

}
